#include <gtest/gtest.h>

#include <diophantus/parser/RPNEquation.h>
#include <diophantus/parser/ParserException.h>

using namespace Diophantus::Parser;

namespace {
    bool TokensEqual(TokenPtr lt, TokenPtr rt) {
        if (lt->tokenType() != rt->tokenType())
            return false;
        if (lt->tokenType() == Token::Type::OPERATOR) {
            if ((bool)dynamic_cast<PlusOperator*>(lt.get()) != (bool)dynamic_cast<PlusOperator*>(rt.get()))
                return false;
            if ((bool)dynamic_cast<MinusOperator*>(lt.get()) != (bool)dynamic_cast<MinusOperator*>(rt.get()))
                return false;
            if ((bool)dynamic_cast<MultiplicationOperator*>(lt.get()) != (bool)dynamic_cast<MultiplicationOperator*>(rt.get()))
                return false;
        }
        if (lt->tokenType() != Token::Type::VALUE)
            return true;
        
        if (dynamic_cast<ConstantToken*>(lt.get())) {
            if (!dynamic_cast<ConstantToken*>(rt.get())) {
                return false;
            }
            int ltValue = dynamic_cast<ConstantToken*>(lt.get())->getValue();
            int rtValue = dynamic_cast<ConstantToken*>(rt.get())->getValue();
            return ltValue == rtValue;
        }

        if (dynamic_cast<VariableToken*>(lt.get())) {
            if (!dynamic_cast<VariableToken*>(rt.get())) {
                return false;
            }
            int ltId = dynamic_cast<VariableToken*>(lt.get())->getId();
            int rtId = dynamic_cast<VariableToken*>(rt.get())->getId();
            return ltId == rtId;
        }

        return false;
    }

    void runParserRPNTest(std::string equation, std::vector<TokenPtr> expectedRPN, std::vector<std::string> expectedVarNames) {
        auto eq = RPNEquation(equation);
        
        ASSERT_EQ(expectedRPN.size(), eq.getRPN().size());
        for (std::size_t i = 0; i != expectedRPN.size(); i++) {
            ASSERT_TRUE(TokensEqual(expectedRPN[i], eq.getRPN()[i]));
        }
        ASSERT_EQ(expectedVarNames, eq.getVariableNames());
        ASSERT_EQ(expectedVarNames.size(), eq.getVariableNumber());
    }
}

TEST(TestParserRPN, testEmptyString) {
    runParserRPNTest("", {}, {});
}

TEST(TestParserRPN, testSpaces) {
    runParserRPNTest("  \t  ", {}, {});
}

TEST(TestParserRPN, testSingleNumber) {
    runParserRPNTest("179",
                     {
                         std::make_shared<ConstantToken>(179)
                     },
                     {});
}

TEST(TestParserRPN, testSingleNumberWithSpaces) {
    runParserRPNTest(" \t\t179\t  ",
                     {
                         std::make_shared<ConstantToken>(179)
                     },
                     {});
}

TEST(TestParserRPN, testSingleVariable) {
    runParserRPNTest("xyz",
                     {
                        std::make_shared<VariableToken>(0)
                     },
                     {
                         "xyz"
                     });
}

TEST(TestParserRPN, testSingleVariableWithSpaces) {
    runParserRPNTest(" \t\txyz\t  ",
                     {
                        std::make_shared<VariableToken>(0)
                     },
                     {
                         "xyz"
                     });
}

TEST(TestParserRPN, testPlusOperatorOnVariablesAndConstants) {
    runParserRPNTest("2  + 3+179 +x + xyz + 4 +xyz",
                     {
                        std::make_shared<ConstantToken>(2),
                        std::make_shared<ConstantToken>(3),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<ConstantToken>(179),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<VariableToken>(0),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<VariableToken>(1),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<ConstantToken>(4),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<VariableToken>(1),
                        std::make_shared<PlusOperator>()
                     },
                     {
                        "x",
                        "xyz"
                     });
}

TEST(TestParserRPN, testMinusOperatorOnVariablesAndConstants) {
    runParserRPNTest("2  - 3-179 -x - xyz - 4 -xyz",
                     {
                        std::make_shared<ConstantToken>(2),
                        std::make_shared<ConstantToken>(3),
                        std::make_shared<MinusOperator>(),
                        std::make_shared<ConstantToken>(179),
                        std::make_shared<MinusOperator>(),
                        std::make_shared<VariableToken>(0),
                        std::make_shared<MinusOperator>(),
                        std::make_shared<VariableToken>(1),
                        std::make_shared<MinusOperator>(),
                        std::make_shared<ConstantToken>(4),
                        std::make_shared<MinusOperator>(),
                        std::make_shared<VariableToken>(1),
                        std::make_shared<MinusOperator>()
                     },
                     {
                        "x",
                        "xyz"
                     });
}

TEST(TestParserRPN, testMultiplicationOperatorOnVariablesAndConstants) {
    runParserRPNTest("2  * 3*179 *x * xyz * 4 *xyz",
                     {
                        std::make_shared<ConstantToken>(2),
                        std::make_shared<ConstantToken>(3),
                        std::make_shared<MultiplicationOperator>(),
                        std::make_shared<ConstantToken>(179),
                        std::make_shared<MultiplicationOperator>(),
                        std::make_shared<VariableToken>(0),
                        std::make_shared<MultiplicationOperator>(),
                        std::make_shared<VariableToken>(1),
                        std::make_shared<MultiplicationOperator>(),
                        std::make_shared<ConstantToken>(4),
                        std::make_shared<MultiplicationOperator>(),
                        std::make_shared<VariableToken>(1),
                        std::make_shared<MultiplicationOperator>()
                     },
                     {
                        "x",
                        "xyz"
                     });
}

TEST(TestParserRPN, testPlusesAndMinusesWithParenthesis) {
    runParserRPNTest("2 + (3 - 4) - (5 + 6 + 7 - 179) + 179 - 10 + (1 - 2 + 3)",
                     {
                        std::make_shared<ConstantToken>(2),
                        std::make_shared<ConstantToken>(3),
                        std::make_shared<ConstantToken>(4),
                        std::make_shared<MinusOperator>(),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<ConstantToken>(5),
                        std::make_shared<ConstantToken>(6),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<ConstantToken>(7),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<ConstantToken>(179),
                        std::make_shared<MinusOperator>(),
                        std::make_shared<MinusOperator>(),
                        std::make_shared<ConstantToken>(179),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<ConstantToken>(10),
                        std::make_shared<MinusOperator>(),
                        std::make_shared<ConstantToken>(1),
                        std::make_shared<ConstantToken>(2),
                        std::make_shared<MinusOperator>(),
                        std::make_shared<ConstantToken>(3),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<PlusOperator>()
                     },
                     {});
}

TEST(TestParserRPN, testPlusesAndMinusesWithNestedParenthesis) {
    runParserRPNTest("2 - (2 + 3 - (179 - 180) + (180 - 179 + (1 + 2 + 3)   ) - (5 + 6)) + 11",
                     {
                        std::make_shared<ConstantToken>(2),
                        std::make_shared<ConstantToken>(2),
                        std::make_shared<ConstantToken>(3),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<ConstantToken>(179),
                        std::make_shared<ConstantToken>(180),
                        std::make_shared<MinusOperator>(),
                        std::make_shared<MinusOperator>(),
                        std::make_shared<ConstantToken>(180),
                        std::make_shared<ConstantToken>(179),
                        std::make_shared<MinusOperator>(),
                        std::make_shared<ConstantToken>(1),
                        std::make_shared<ConstantToken>(2),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<ConstantToken>(3),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<ConstantToken>(5),
                        std::make_shared<ConstantToken>(6),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<MinusOperator>(),
                        std::make_shared<MinusOperator>(),
                        std::make_shared<ConstantToken>(11),
                        std::make_shared<PlusOperator>()
                     },
                     {});
}

TEST(TestParserRPN, testPlusesMinusesMultiplicationsPriorities) {
    runParserRPNTest("2 + 3 * 4 - 4 * 3 * 5 + 5",
                     {
                        std::make_shared<ConstantToken>(2),
                        std::make_shared<ConstantToken>(3),
                        std::make_shared<ConstantToken>(4),
                        std::make_shared<MultiplicationOperator>(),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<ConstantToken>(4),
                        std::make_shared<ConstantToken>(3),
                        std::make_shared<MultiplicationOperator>(),
                        std::make_shared<ConstantToken>(5),
                        std::make_shared<MultiplicationOperator>(),
                        std::make_shared<MinusOperator>(),
                        std::make_shared<ConstantToken>(5),
                        std::make_shared<PlusOperator>()
                     },
                     {});
}

TEST(TestParserRPN, testPlusesMinusesMultiplicationsPrioritiesWithNestedParenthesis) {
    runParserRPNTest("2 * (3 + 4 * (5 + 6) - (4 * 3) - 5 * (6 + 7 - 8) * 123) - 100",
                     {
                        std::make_shared<ConstantToken>(2),
                        std::make_shared<ConstantToken>(3),
                        std::make_shared<ConstantToken>(4),
                        std::make_shared<ConstantToken>(5),
                        std::make_shared<ConstantToken>(6),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<MultiplicationOperator>(),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<ConstantToken>(4),
                        std::make_shared<ConstantToken>(3),
                        std::make_shared<MultiplicationOperator>(),
                        std::make_shared<MinusOperator>(),
                        std::make_shared<ConstantToken>(5),
                        std::make_shared<ConstantToken>(6),
                        std::make_shared<ConstantToken>(7),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<ConstantToken>(8),
                        std::make_shared<MinusOperator>(),
                        std::make_shared<MultiplicationOperator>(),
                        std::make_shared<ConstantToken>(123),
                        std::make_shared<MultiplicationOperator>(),
                        std::make_shared<MinusOperator>(),
                        std::make_shared<MultiplicationOperator>(),
                        std::make_shared<ConstantToken>(100),
                        std::make_shared<MinusOperator>()
                     },
                     {});
}

TEST(TestParserRPN, testAllKindsOfTokens) {
    runParserRPNTest("2 * (x + y) - xyz + 179 * 10 - y",
                     {
                        std::make_shared<ConstantToken>(2),
                        std::make_shared<VariableToken>(0),
                        std::make_shared<VariableToken>(1),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<MultiplicationOperator>(),
                        std::make_shared<VariableToken>(2),
                        std::make_shared<MinusOperator>(),
                        std::make_shared<ConstantToken>(179),
                        std::make_shared<ConstantToken>(10),
                        std::make_shared<MultiplicationOperator>(),
                        std::make_shared<PlusOperator>(),
                        std::make_shared<VariableToken>(1),
                        std::make_shared<MinusOperator>()
                     },
                     {
                        "x",
                        "y",
                        "xyz"
                     });
}

TEST(TestParserRPN, testMismatchedLeftParen) {
    ASSERT_THROW(RPNEquation("(2 + 3 * (4 + 198) - 1"), ParserException); 
}

TEST(TestParserRPN, testMismatchedRightParen) {
    ASSERT_THROW(RPNEquation("2 + 3 * (4 + 198 * x) - 1)"), ParserException);
}

TEST(TestParserRPN, testNonexistentOperatorToken) {
    ASSERT_THROW(RPNEquation("2 / 3"), ParserException);
}

TEST(TestParserRPN, testBigNumber) {
    ASSERT_THROW(RPNEquation("179179179179"), ParserException);
}
