#include <gtest/gtest.h>

#include <diophantus/parser/Parse.h>
#include <diophantus/parser/ParserException.h>

using namespace Diophantus::Parser;

TEST(TestParserEvaluate, testSingleNumber) {
    ASSERT_EQ(2, Parse("2")->evaluate({}));
}

TEST(TestParserEvaluate, testSimpleAddition) {
    ASSERT_EQ(4, Parse("2 + 2")->evaluate({}));
}

TEST(TestParserEvaluate, testSimpleSubstraction) {
    ASSERT_EQ(-17, Parse("2 - 19")->evaluate({}));
}

TEST(TestParserEvaluate, testSimpleMultiplication) {
    ASSERT_EQ(34, Parse("2 * 17")->evaluate({}));
}

TEST(TestParserEvaluate, testSingleVariable) {
    ASSERT_EQ(2, Parse("xyz")->evaluate({2}));
}

TEST(TestParserEvaluate, testVariableAddition) {
    ASSERT_EQ(63, Parse("x + y + x")->evaluate({23, 17}));
}

TEST(TestParserEvaluate, testVariableSubstraction) {
    ASSERT_EQ(23, Parse("63 - x - y")->evaluate({23, 17}));
}

TEST(TestParserEvaluate, testVariableMultiplication) {
    ASSERT_EQ(230, Parse("x * y")->evaluate({23, 10}));
}

TEST(TestParserEvaluate, testOperationPriorities) {
    ASSERT_EQ(6, Parse("2 + x * 2")->evaluate({2}));
}

TEST(TestParserEvaluate, testIncorrectOperatorUsageTooManyValues) {
    ASSERT_THROW(Parse("2 x"), ParserException);
}

TEST(TestParserEvaluate, testComplexEquation) {
    Diophantus::EquationPtr equation = Parse("2 * (x + y + xyz) - 179 * x * (7 + (1 + 2 + (3 * 4 + 5) * 6))");
    std::vector<std::string> names = {"x", "y", "xyz"};

    ASSERT_EQ(3, equation->variableNumber());
    ASSERT_EQ(names, equation->variableNames());
    ASSERT_EQ(-40078, equation->evaluate({2, 3, 4}));
}
