#pragma once

#include <diophantus/Equation.h>

template<class Mock>
class TestEnvEquation : public Diophantus::Equation {
    std::vector<std::string> names_;
    Mock mock_;
public:
    TestEnvEquation(std::vector<std::string> names, Mock mock)
        : names_(names), mock_(mock) {}
    virtual ~TestEnvEquation() {}

    virtual int variableNumber() const override {
        return names_.size();
    }

    virtual const std::vector<std::string>& variableNames() const override {
        return names_;
    }

    virtual int evaluate(const std::vector<int>& values) const override {
        return mock_(values);
    }
};

template<class Mock>
Diophantus::EquationPtr MakeEquation(std::vector<std::string> names, Mock mock) {
    return std::make_shared<TestEnvEquation<Mock>>(names, mock);
}
