#include <gtest/gtest.h>

#include <diophantus/parser/Token.h>

#include <algorithm>

using namespace Diophantus::Parser;

TEST(TestParserTokens, testTokenOperatorPriorities) {
    TokenPtr constant = std::make_shared<ConstantToken>(0);
    TokenPtr variable = std::make_shared<VariableToken>(0);
    TokenPtr plusOperator = std::make_shared<PlusOperator>();
    TokenPtr minusOperator = std::make_shared<MinusOperator>();
    TokenPtr multiplicationOperator = std::make_shared<MultiplicationOperator>();
    TokenPtr leftParen = std::make_shared<LeftParen>();
    TokenPtr rightParen = std::make_shared<RightParen>();

    ASSERT_EQ(plusOperator->operatorPriority(), minusOperator->operatorPriority());
    ASSERT_LT(multiplicationOperator->operatorPriority(), plusOperator->operatorPriority());
    ASSERT_EQ(-1, constant->operatorPriority());
    ASSERT_EQ(-1, variable->operatorPriority());
    ASSERT_EQ(-1, leftParen->operatorPriority());
    ASSERT_EQ(-1, rightParen->operatorPriority());
}

TEST(TestParserTokens, testTokenTypes) {
    TokenPtr constant = std::make_shared<ConstantToken>(0);
    TokenPtr variable = std::make_shared<VariableToken>(0);
    TokenPtr plusOperator = std::make_shared<PlusOperator>();
    TokenPtr minusOperator = std::make_shared<MinusOperator>();
    TokenPtr multiplicationOperator = std::make_shared<MultiplicationOperator>();
    TokenPtr leftParen = std::make_shared<LeftParen>();
    TokenPtr rightParen = std::make_shared<RightParen>();

    ASSERT_EQ(Token::Type::VALUE, constant->tokenType());
    ASSERT_EQ(Token::Type::VALUE, variable->tokenType());
    ASSERT_EQ(Token::Type::OPERATOR, plusOperator->tokenType());
    ASSERT_EQ(Token::Type::OPERATOR, minusOperator->tokenType());
    ASSERT_EQ(Token::Type::OPERATOR, multiplicationOperator->tokenType());
    ASSERT_EQ(Token::Type::LEFT_PAREN, leftParen->tokenType());
    ASSERT_EQ(Token::Type::RIGHT_PAREN, rightParen->tokenType());
}

namespace {
    void testTokenBehaviour(TokenPtr token, std::vector<int> initStack, std::vector<int> values, std::vector<int> expectedStack) {
        std::stack<int> stack;
        for (int u : initStack)
            stack.push(u);

        token->evaluate(stack, values);

        std::vector<int> actualStack;
        while (!stack.empty()) {
            actualStack.push_back(stack.top());
            stack.pop();
        }
        std::reverse(actualStack.begin(), actualStack.end());

        ASSERT_EQ(expectedStack, actualStack);
    }
}

TEST(TestParserTokens, testConstantTokenBehaviour) {
    TokenPtr constant = std::make_shared<ConstantToken>(179);

    testTokenBehaviour(constant, {1, 2, 3}, {}, {1, 2, 3, 179});
    testTokenBehaviour(constant, {}, {}, {179});
}

TEST(TestParserTokens, testVariableTokenBehaviour) {
    TokenPtr first = std::make_shared<VariableToken>(0);
    TokenPtr second = std::make_shared<VariableToken>(1);

    testTokenBehaviour(first, {1, 2, 3}, {179, 239}, {1, 2, 3, 179});
    testTokenBehaviour(second, {}, {179, 239}, {239});
}

TEST(TestParserTokens, testPlusOperatorBehaviour) {
    TokenPtr plusOperator = std::make_shared<PlusOperator>();

    testTokenBehaviour(plusOperator, {1, 2, 3}, {}, {1, 5});
    testTokenBehaviour(plusOperator, {-1, 1}, {}, {0});
}

TEST(TestParserTokens, testMinusOperatorBehaviour) {
    TokenPtr minusOperator = std::make_shared<MinusOperator>();
    
    testTokenBehaviour(minusOperator, {1, 2, 3}, {}, {1, -1});
    testTokenBehaviour(minusOperator, {-1, 1}, {}, {-2});
}

TEST(TestParserTokens, testMultiplicationOperatorBehaviour) {
    TokenPtr multiplicationOperator = std::make_shared<MultiplicationOperator>();

    testTokenBehaviour(multiplicationOperator, {1, 2, 3}, {}, {1, 6});
    testTokenBehaviour(multiplicationOperator, {-1, 1}, {}, {-1});
}

TEST(TestParserTokens, testLeftParenBehaviour) {
    TokenPtr leftParen = std::make_shared<LeftParen>();

    testTokenBehaviour(leftParen, {1, 2, 3}, {}, {1, 2, 3});
}

TEST(TestParserTokens, testRightParenBehaviour) {
    TokenPtr rightParen = std::make_shared<RightParen>();

    testTokenBehaviour(rightParen, {1, 2, 3}, {}, {1, 2, 3});
}
