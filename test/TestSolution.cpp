#include <gtest/gtest.h>

#include <test/TestEnvEquation.h>

#include <diophantus/CurrentLaunch.h>
#include <diophantus/Solution.h>

#include <helicase/lib/BitString.h>

using namespace Diophantus;
using namespace Helicase;

namespace {
    EquationPtr equation = MakeEquation(
        {"x", "y", "z"},
        [](const std::vector<int>& values) -> int {
            return values[0] * values[1] * values[1] + values[2] - 179;
        });

    Solution zeroes({0, 0, 0});
    Solution realSolution({1, 10, 79});
}

TEST(TestSolution, testSolutionEvaluation) {
    CurrentLaunch::getInstance().equation = equation;
    
    ASSERT_EQ(-179, zeroes.getValue());
    ASSERT_LE(std::abs(1.0 / 179 - zeroes.getFitness()), 1e-9);
    ASSERT_EQ(0, realSolution.getValue());
}

TEST(TestSolution, testSolutionTransformation) {
    CurrentLaunch::getInstance().equation = equation;
    
    ASSERT_EQ(zeroes.getBinaryGenotypeSize(), realSolution.getBinaryGenotypeSize());
    ASSERT_EQ(zeroes.getValues(), Solution(zeroes.getBinaryGenotype()).getValues());
    ASSERT_EQ(realSolution.getValues(), Solution(realSolution.getBinaryGenotype()).getValues());
}
