#include <gtest/gtest.h>

#include <diophantus/Engine.h>

using namespace Diophantus;

TEST(TestEngine, testInvocationSuccess) {
    SolutionPtr solution = std::make_shared<Solution>(std::vector<int>());
    ASSERT_TRUE(invocationSuccess(solution, 179)->success());
    ASSERT_EQ(179, invocationSuccess(solution, 179)->generations());
    ASSERT_EQ(solution, invocationSuccess(solution, 179)->solution());
}

TEST(TestEngine, testInvocationFail) {
    ASSERT_FALSE(invocationFail()->success());
    ASSERT_THROW(invocationFail()->generations(), FailedInvocationException);
    ASSERT_THROW(invocationFail()->solution(), FailedInvocationException); 
}
