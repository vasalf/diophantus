#pragma once

#include <memory>

namespace Diophantus {

class Mode {
public:
    virtual ~Mode() = default;
    virtual void runMode() = 0;
};

typedef std::shared_ptr<Mode> ModePtr;

}
