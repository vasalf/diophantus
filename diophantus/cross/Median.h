#pragma once

#include <diophantus/Solution.h>

#include <helicase/cross/CrossEngine.h>

namespace Diophantus {
namespace Cross {

class Median : public Helicase::CrossEngine<Diophantus::Solution> {
public:
    Median() {}
    virtual ~Median() {}
    
    virtual SolutionPtr cross(SolutionPtr father, SolutionPtr mother);
};

}
}
