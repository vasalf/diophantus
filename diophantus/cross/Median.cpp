#include <diophantus/cross/Median.h>

#include <diophantus/CurrentLaunch.h>

#include <memory>

using Diophantus::SolutionPtr;

namespace Diophantus {
namespace Cross {

SolutionPtr Median::cross(SolutionPtr father, SolutionPtr mother) {
    int vars = CurrentLaunch::getInstance().equation->variableNumber();
    std::vector<int> res(vars);
    
    for (int i = 0; i < vars; i++) {
        int x = father->getValues()[i], y = mother->getValues()[i];
        res[i] = (x + y) / 2;
    }

    return std::make_shared<Solution>(res);
}

}
}
