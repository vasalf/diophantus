#include <diophantus/Engine.h>

namespace Diophantus {

class SuccessfulInvocation : public InvocationResult {
    SolutionPtr solution_;
    int generations_;
public:
    SuccessfulInvocation(SolutionPtr solution, int generations) 
        : solution_(solution), generations_(generations) {}

    virtual bool success() const override final {
        return true;
    }

    virtual int generations() const override final {
        return generations_;
    }

    virtual SolutionPtr solution() const override final {
        return solution_;
    }
};

class FailedInvocation : public InvocationResult {
public:
    FailedInvocation() {}

    virtual bool success() const override final {
        return false;   
    }

    virtual int generations() const override final {
        throw FailedInvocationException();
    }

    virtual SolutionPtr solution() const override final {
        throw FailedInvocationException();
    }
};

InvocationResultPtr invocationSuccess(SolutionPtr solution, int generations) {
    return std::make_shared<SuccessfulInvocation>(solution, generations);
}

InvocationResultPtr invocationFail() {
    return std::make_shared<FailedInvocation>();
}

}
