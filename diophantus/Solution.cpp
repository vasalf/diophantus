#include <diophantus/Solution.h>

#include <diophantus/CurrentLaunch.h>

namespace Diophantus {

Solution::Solution(const Helicase::BitString& binaryGenotype) {
    values_.resize(CurrentLaunch::getInstance().equation->variableNumber());
    int j = 0;
    for (std::size_t i = 0; i < values_.size(); i++) {
        for (int bit = 0; bit < 32; bit++)
            values_[i] |= binaryGenotype[j++] << bit;
    }
}

int Solution::getBinaryGenotypeSize() const {
    return 32 * CurrentLaunch::getInstance().equation->variableNumber();
}

Helicase::BitString Solution::getBinaryGenotype() const {
    Helicase::BitString ret(getBinaryGenotypeSize());

    int j = 0;
    for (int i = 0; i < CurrentLaunch::getInstance().equation->variableNumber(); i++) {
        for (int bit = 0; bit < 32; bit++) {
            ret[j++] = (values_[i] >> bit) & 1;
        }
    }

    return ret;
}

}
