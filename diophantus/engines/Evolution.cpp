#include <diophantus/engines/Evolution.h>

#include <diophantus/CurrentLaunch.h>

#include <helicase/Evolve.h>

#include <algorithm>
#include <random>

using namespace Diophantus;

namespace Diophantus {
namespace Engines {

class Evolution::TImpl {
    int populationSize_;
    int alivers_;
    int generationLimit_;
    Helicase::CrossEnginePtr<Solution> crossEngine_;
    Helicase::MutationEnginePtr<Solution> mutationEngine_;
    mutable std::mt19937 rnd_;

    SolutionPtr genRandomSolution() const {
        std::vector<int> values(CurrentLaunch::getInstance().equation->variableNumber());
        std::generate(values.begin(), values.end(), rnd_);
        return std::make_shared<Solution>(values);
    }

public:
    TImpl(int populationSize,
          int alivers,
          int generationLimit,
          int rndSeed,
          Helicase::CrossEnginePtr<Solution> crossEngine,
          Helicase::MutationEnginePtr<Solution> mutationEngine)
          : populationSize_(populationSize),
            alivers_(alivers),
            generationLimit_(generationLimit),
            crossEngine_(crossEngine),
            mutationEngine_(mutationEngine) {
        if (rndSeed == 0) {
            rndSeed = std::random_device()();
        } 
        rnd_ = std::mt19937(rndSeed);
    }

    InvocationResultPtr invoke() const {
        const LoggerPtr& logger = CurrentLaunch::getInstance().logger;
        std::vector<SolutionPtr> population;
        population.reserve(populationSize_);
        for (int i = 0; i < populationSize_; i++) {
            population.push_back(genRandomSolution());
            if (population[i]->getValue() == 0) {
                // We are really lucky this time
                return invocationSuccess(population[i], 0);   
            }
        }
        if (logger) {
            logger->logPopulation(population);
        }
        
        Helicase::Params<Solution, std::mt19937> params = {
            .crossEngine = crossEngine_,
            .mutationEngine = mutationEngine_,
            .alivers = alivers_,
            .rndEng = rnd_
        };
        for (int generation = 1; generation <= generationLimit_; generation++) {
            population = Helicase::evolve<Solution, std::mt19937>(population, params);
            if (logger) {
                logger->logPopulation(population);
            }
            for (const auto& solution : population) {
                if (solution->getValue() == 0) {
                    return invocationSuccess(solution, generation);
                }
            }
        }
        return invocationFail();
    }
};

Evolution::Evolution(std::string name,
                     int populationSize,
                     int alivers,
                     int generationLimit,
                     int rndSeed,
                     Helicase::CrossEnginePtr<Solution> crossEngine,
                     Helicase::MutationEnginePtr<Solution> mutationEngine) : name_(name) {
    impl_ = std::make_shared<Evolution::TImpl>(populationSize, alivers, generationLimit, rndSeed, crossEngine, mutationEngine);
}

InvocationResultPtr Evolution::invoke() const {
    return impl_->invoke();
}

}
}
