#pragma once

#include <diophantus/Engine.h>

#include <helicase/cross/CrossEngine.h>
#include <helicase/mutation/MutationEngine.h>

#include <memory>

namespace Diophantus {
namespace Engines {

class Evolution : public Diophantus::Engine {
    class TImpl;
    std::shared_ptr<TImpl> impl_;
    std::string name_;
public:
    Evolution(std::string name,
              int populationSize,
              int alivers,
              int generationLimit,
              int rndSeed,
              Helicase::CrossEnginePtr<Diophantus::Solution> crossEngine,
              Helicase::MutationEnginePtr<Diophantus::Solution> mutationEngine);
    
    Evolution(const Evolution&) = delete;
    Evolution& operator=(const Evolution&) = delete;
    
    Evolution(Evolution&& other) {
        impl_ = std::move(other.impl_);
    }

    Evolution& operator=(Evolution&& other) {
        impl_ = std::move(other.impl_);
        return *this;
    }

    inline virtual std::string getName() const override {
        return name_;
    }

    virtual Diophantus::InvocationResultPtr invoke() const override;
};

}
}
