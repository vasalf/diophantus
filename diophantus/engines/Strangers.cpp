#include <diophantus/engines/Strangers.h>

#include <diophantus/CurrentLaunch.h>

#include <helicase/Evolve.h>

#include <algorithm>
#include <cmath>
#include <random>

using namespace Diophantus;

namespace Diophantus {
namespace Engines {

class Strangers::TImpl {
    int initialSize_;
    double alivers_;
    int periodLength_;
    int periodicAddition_;
    int periodsLimit_;
    Helicase::CrossEnginePtr<Solution> crossEngine_;
    Helicase::MutationEnginePtr<Solution> mutationEngine_;
    mutable std::mt19937 rnd_;

    SolutionPtr genRandomSolution() const {
        std::vector<int> values(CurrentLaunch::getInstance().equation->variableNumber());
        std::generate(values.begin(), values.end(), rnd_);
        return std::make_shared<Solution>(values);
    }

public:
    TImpl(int initialSize,
          double alivers,
          int periodLength,
          int periodicAddition,
          int periodsLimit,
          int rndSeed,
          Helicase::CrossEnginePtr<Diophantus::Solution> crossEngine,
          Helicase::MutationEnginePtr<Diophantus::Solution> mutationEngine)
          : initialSize_(initialSize),
            alivers_(alivers),
            periodLength_(periodLength),
            periodicAddition_(periodicAddition),
            periodsLimit_(periodsLimit),
            crossEngine_(crossEngine),
            mutationEngine_(mutationEngine) {
        if (rndSeed == 0) {
            rndSeed = std::random_device()();
        }
        rnd_ = std::mt19937(rndSeed);
    }

    InvocationResultPtr invoke() const {
        const LoggerPtr& logger = CurrentLaunch::getInstance().logger;
        std::vector<SolutionPtr> population;
        population.reserve(initialSize_ + periodsLimit_ * periodicAddition_);
        for (int i = 0; i < initialSize_; i++) {
            population.push_back(genRandomSolution());
            if (population[i]->getValue() == 0) {
                return invocationSuccess(population[i], 0);
            }
        }
        if (logger) {
            logger->logPopulation(population);
        }

        Helicase::Params<Solution, std::mt19937> params = {
            .crossEngine = crossEngine_,
            .mutationEngine = mutationEngine_,
            .alivers = (int)std::floor(alivers_ * initialSize_),
            .rndEng = rnd_
        };
        int curGeneration = 1;
        for (int period = 1; period <= periodsLimit_; period++) {
            for (int generation = 1; generation <= periodLength_; generation++) {
                population = Helicase::evolve<Solution, std::mt19937>(population, params);
                if (logger) {
                    logger->logPopulation(population);
                }
                for (const auto& solution: population) {
                    if (solution->getValue() == 0) {
                        return invocationSuccess(solution, generation);
                    }
                }
                curGeneration++;
            }
            if (period < periodsLimit_) {
                for (int ind = 1; ind <= periodicAddition_; ind++) {
                    population.push_back(genRandomSolution());
                    if (population.back()->getValue() == 0) {
                        return invocationSuccess(population.back(), curGeneration);
                    }
                }
                params.alivers = (int)std::floor(alivers_ * population.size());
            }
        }
        return invocationFail();
    }
};

Strangers::Strangers(std::string name, 
                     int initialSize,
                     double alivers,
                     int periodLength,
                     int periodicAddition,
                     int periodsLimit,
                     int rndSeed,
                     Helicase::CrossEnginePtr<Diophantus::Solution> crossEngine,
                     Helicase::MutationEnginePtr<Diophantus::Solution> mutationEngine) : name_(name) {
        impl_ = std::make_shared<Strangers::TImpl>(initialSize, alivers, periodLength, periodicAddition, periodsLimit, rndSeed, crossEngine, mutationEngine);
}

InvocationResultPtr Strangers::invoke() const {
    return impl_->invoke();
}

}
}
