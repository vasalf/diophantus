#pragma once

#include <diophantus/Engine.h>

#include <helicase/cross/CrossEngine.h>
#include <helicase/mutation/MutationEngine.h>

#include <memory>
#include <vector>

namespace Diophantus {
namespace Engines {

class Strangers : public Diophantus::Engine {
    class TImpl;
    std::shared_ptr<TImpl> impl_;
    std::string name_;
public:
    Strangers(std::string name,
              int initialSize,
              double alivers,
              int periodLength,
              int periodicAddition,
              int periodsLimit,
              int rndSeed,
              Helicase::CrossEnginePtr<Diophantus::Solution> crossEngine,
              Helicase::MutationEnginePtr<Diophantus::Solution> mutationEngine);

    Strangers(const Strangers&) = delete;
    Strangers& operator=(const Strangers&) = delete;

    Strangers(Strangers&& other) {
        impl_ = std::move(other.impl_);
    }

    Strangers& operator=(Strangers&& other) {
        impl_ = std::move(other.impl_);
        return *this;
    }

    inline virtual std::string getName() const override {
        return name_;
    }

    virtual Diophantus::InvocationResultPtr invoke() const override;
};

}
}
