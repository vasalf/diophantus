#include <diophantus/Config.h>

#include <diophantus/cross/Median.h>
#include <diophantus/engines/Evolution.h>
#include <diophantus/engines/Strangers.h>
#include <diophantus/mutation/NormalRandom.h>
#include <diophantus/parser/Parse.h>
#include <diophantus/CurrentLaunch.h>
#include <diophantus/Engine.h>
#include <diophantus/Solution.h>
#include <diophantus/SolveMode.h>

#include <helicase/cross/CrossingOverCrossEngine.h>
#include <helicase/cross/ReturnFatherCrossEngine.h>
#include <helicase/mutation/BitFlipMutationEngine.h>
#include <helicase/mutation/DoNothingMutationEngine.h>

#include <rapidjson/document.h>
#include <rapidjson/error/en.h>
#include <rapidjson/istreamwrapper.h>

#include <random>

namespace Diophantus {

Helicase::CrossEnginePtr<Solution> parseCrossingOverConfig(rapidjson::Value& config) {
    int seed;
    if (!config.HasMember("rnd_seed")) {
        seed = std::random_device()();
    } else {
        if (!config["rnd_seed"].IsInt()) {
            throw ConfigException("random seed must be an integer");
        }
        seed = config["rnd_seed"].GetInt();
    }
    if (!config.HasMember("repeat")) {
        throw ConfigException("crossing over repeat number is not specified");
    }
    if (!config["repeat"].IsInt() || config["repeat"].GetInt() < 0) {
        throw ConfigException("crossing over repeat number must be a non-negative integer");
    }
    std::mt19937 rnd(seed);
    return std::make_shared<Helicase::CrossingOverCrossEngine<std::mt19937, Solution>>(rnd, config["repeat"].GetInt());
}

Helicase::CrossEnginePtr<Solution> parseCrossEngineConfig(rapidjson::Value& config) {
    if (!config.HasMember("type")) {
        throw ConfigException("no cross engine type specified");
    }
    if (!config["type"].IsString()) {
        throw ConfigException("cross engine type must be a string");
    }

    if (std::string(config["type"].GetString()) == "return_father") {
        return std::make_shared<Helicase::ReturnFatherCrossEngine<Solution>>();
    } else if (std::string(config["type"].GetString()) == "crossing_over") {
        return parseCrossingOverConfig(config);
    } else if (std::string(config["type"].GetString()) == "median") {
        return std::make_shared<Cross::Median>();
    } else {
        throw ConfigException("unknown cross engine type: " + std::string(config["type"].GetString()));
    }
}

Helicase::MutationEnginePtr<Solution> parseBitFlipConfig(rapidjson::Value& config) {
    int seed;
    if (!config.HasMember("rnd_seed")) {
        seed = std::random_device()();
    } else {
        if (!config["rnd_seed"].IsInt()) {
            throw ConfigException("random seed must be an integer");
        }
        seed = config["rnd_seed"].GetInt();
    }
    if (!config.HasMember("rate")) {
        throw ConfigException("no bit flip rate specified");
    }
    if (!config["rate"].IsNumber() || config["rate"].GetDouble() < 0 || config["rate"].GetDouble() > 1) {
        throw ConfigException("bit flip rate must be a number between 0 and 1");
    }
    std::mt19937 rnd(seed);
    return std::make_shared<Helicase::BitFlipMutationEngine<std::mt19937, Solution>>(rnd, config["rate"].GetDouble());
}   

Helicase::MutationEnginePtr<Solution> parseNormalRandomMutationConfig(rapidjson::Value& config) {
    int rndSeed = 0;
    if (config.HasMember("rnd_seed")) {
        if (!config["rnd_seed"].IsInt()) {
            throw ConfigException("random seed must be an integer");
        }
        rndSeed = config["rnd_seed"].GetInt();
    }

    if (!config.HasMember("deviation")) {
        throw ConfigException("no standard deviation specified");
    }
    if (!config["deviation"].IsNumber() || config["deviation"].GetDouble() <= 0) {
        throw ConfigException("standard deviation must be a positive number");
    }

    return std::make_shared<Mutation::NormalRandom>(rndSeed, config["deviation"].GetDouble());
}

Helicase::MutationEnginePtr<Solution> parseMutationEngineConfig(rapidjson::Value& config) {
    if (!config.HasMember("type")) {
        throw ConfigException("mutation engine type is not specified");
    } else if (!config["type"].IsString()) {
        throw ConfigException("mutation engine type must be a string");
    }

    if (std::string(config["type"].GetString()) == "do_nothing") {
        return std::make_shared<Helicase::DoNothingMutationEngine<Solution>>();
    } else if (std::string(config["type"].GetString()) == "bit_flip") {
        return parseBitFlipConfig(config);
    } else if (std::string(config["type"].GetString()) == "normal_random") {
        return parseNormalRandomMutationConfig(config);
    } else {
        throw ConfigException("unknown mutation engine type: " + std::string(config["type"].GetString()));
    }
}

EnginePtr parseEvolutionEngineConfig(rapidjson::Value& config) {
    if (!config.HasMember("name")) {
        throw ConfigException("no engine name specified");
    }
    if (!config["name"].IsString()) {
        throw ConfigException("engine name must be a string");
    }
    if (!config.HasMember("population_size")) {
        throw ConfigException("population size is not specified");
    }
    if (!config["population_size"].IsInt() || config["population_size"].GetInt() <= 0) {
        throw ConfigException("population size must be a positive integer");
    }
    if (!config.HasMember("alivers")) {
        throw ConfigException("alivers number is not specified");
    }
    if (!config["alivers"].IsInt() || config["alivers"].GetInt() < 0) {
        throw ConfigException("alivers number must be a non-negative integer");
    }
    if (!config.HasMember("generation_limit")) {
        throw ConfigException("limit for number of generations is not specified");
    }
    if (!config["generation_limit"].IsInt() || config["generation_limit"].GetInt() < 0) {
        throw ConfigException("limit for number of generations must be a non-negative integer");
    }
    int seed = 0;
    if (config.HasMember("rnd_seed")) {
        if (!config["rnd_seed"].IsInt())
            throw ConfigException("random seed must be an integer");
        seed = config["rnd_seed"].GetInt();
    }
    if (!config.HasMember("cross_engine")) {
        throw ConfigException("cross engine is not specified");
    }
    if (!config["cross_engine"].IsObject()) {
        throw ConfigException("cross engine must be an object");
    }
    if (!config.HasMember("mutation_engine")) {
        throw ConfigException("mutation engine is not specified");
    }
    if (!config["mutation_engine"].IsObject()) {
        throw ConfigException("mutation engine must be an object");
    }
    return std::make_shared<Engines::Evolution>(config["name"].GetString(),
                                                config["population_size"].GetInt(),
                                                config["alivers"].GetInt(),
                                                config["generation_limit"].GetInt(),
                                                seed,
                                                parseCrossEngineConfig(config["cross_engine"]),
                                                parseMutationEngineConfig(config["mutation_engine"]));
}

EnginePtr parseStrangersEngineConfig(rapidjson::Value& config) {
    if (!config.HasMember("name")) {
        throw ConfigException("no engine name specified");
    }
    if (!config["name"].IsString()) {
        throw ConfigException("engine name must be a string");
    }
    if (!config.HasMember("initial_size")) {
        throw ConfigException("initial population size is not specified");
    }
    if (!config["initial_size"].IsInt() || config["initial_size"].GetInt() <= 0) {
        throw ConfigException("initial population size must be a positive integer");
    }
    if (!config.HasMember("alivers")) {
        throw ConfigException("no alivers ratio specified");
    }
    if (!config["alivers"].IsNumber() || config["alivers"].GetDouble() < 0 || config["alivers"].GetDouble() > 1) {
        throw ConfigException("alivers ratio must be a number between 0 and 1");
    }
    if (!config.HasMember("period_length")) {
        throw ConfigException("no period length specified");
    }
    if (!config["period_length"].IsInt() || config["period_length"].GetInt() <= 0) {
        throw ConfigException("period length must be a positive integer");
    }
    if (!config.HasMember("periodic_addition")) {
        throw ConfigException("no periodic addition specified");
    }
    if (!config["periodic_addition"].IsInt() || config["periodic_addition"].GetInt() < 0) {
        throw ConfigException("periodic addition must be a non-negative integer");
    }
    if (!config.HasMember("periods_limit")) {
        throw ConfigException("no periods limit specified");
    }
    if (!config["periods_limit"].IsInt() || config["periods_limit"].GetInt() <= 0) {
        throw ConfigException("periods limit must be a positive integer");
    }
    int rndSeed = 0;
    if (config.HasMember("rnd_seed")) {
        if (!config["rnd_seed"].IsInt()) {
            throw ConfigException("random seed must be an integer");
        }
        rndSeed = config["rnd_seed"].GetInt();
    }
    if (!config.HasMember("cross_engine")) {
        throw ConfigException("no cross engine specified");
    }
    if (!config["cross_engine"].IsObject()) {
        throw ConfigException("cross engine must be an object");
    }
    if (!config.HasMember("mutation_engine")) {
        throw ConfigException("no mutation engine specified");
    }
    if (!config["mutation_engine"].IsObject()) {
        throw ConfigException("mutation engine must be an object");
    }
    return std::make_shared<Engines::Strangers>(config["name"].GetString(),
                                                config["initial_size"].GetInt(),
                                                config["alivers"].GetDouble(),
                                                config["period_length"].GetInt(),
                                                config["periodic_addition"].GetInt(),
                                                config["periods_limit"].GetInt(),
                                                rndSeed,
                                                parseCrossEngineConfig(config["cross_engine"]),
                                                parseMutationEngineConfig(config["mutation_engine"]));
}

EnginePtr parseEngineConfig(rapidjson::Value& config) {
    if (!config.HasMember("type")) {
        throw ConfigException("no engine type specified");
    }
    if (!config["type"].IsString()) {
        throw ConfigException("engine type must be a string");
    }

    if (std::string(config["type"].GetString()) == "evolution") {
        return parseEvolutionEngineConfig(config);
    } else if (std::string(config["type"].GetString()) == "strangers") {
        return parseStrangersEngineConfig(config);
    } else {
        throw ConfigException("unknown engine type: " + std::string(config["type"].GetString()));
    }
}

ModePtr parseSolveConfig(rapidjson::Document& document) {
    if (!document.HasMember("engine")) {
        throw ConfigException("no engine specified");
    }
    if (!document["engine"].IsObject()) {
        throw ConfigException("engine is not an object");
    }
    EnginePtr engine = parseEngineConfig(document["engine"]);
    
    return std::make_shared<SolveMode>(engine);
}

ModePtr parseConfig(std::istream& config) {
    rapidjson::Document document;
    
    rapidjson::IStreamWrapper isw(config);
    document.ParseStream(isw);
    if (document.HasParseError()) {
        throw ConfigException("json: " + std::string(rapidjson::GetParseError_En(document.GetParseError()))); 
    }

    if (!document.IsObject()) {
        throw ConfigException("json root is not an object");
    }

    if (!document.HasMember("equation")) {
        throw ConfigException("no equation specified");
    }
    if (!document["equation"].IsString()) {
        throw ConfigException("equation must be a string");
    }
    CurrentLaunch::getInstance().equation = Parser::Parse(document["equation"].GetString());
    
    bool dbgEnabled = false;
    if (document.HasMember("dbg")) {
        if (!document["dbg"].IsBool()) {
            throw ConfigException("dbg flag must be a boolean");
        }
        dbgEnabled = document["dbg"].GetBool();
    }
    CurrentLaunch::getInstance().logger = std::make_shared<Logger>(dbgEnabled);

    if (!document.HasMember("mode")) {
        throw ConfigException("no mode specified");
    }
    if (!document["mode"].IsString()) {
        throw ConfigException("mode must be a string");
    }
    
    if (std::string(document["mode"].GetString()) == "solve") {
        return parseSolveConfig(document);    
    } else {
        throw ConfigException("unknown mode: " + std::string(document["mode"].GetString()));
    }
}

}
