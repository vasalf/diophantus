#include <diophantus/SolveMode.h>

#include <diophantus/CurrentLaunch.h>

#include <iostream>

namespace Diophantus {

void SolveMode::runMode() {
    std::cout << "running engine " << engine_->getName() << std::endl;
    auto result = engine_->invoke();
    if (!result->success()) {
        std::cout << "no solution found" << std::endl;
    } else {
        std::cout << "solution found in " << result->generations() << " generations" << std::endl;
        for (int i = 0; i < CurrentLaunch::getInstance().equation->variableNumber(); i++) {
            std::cout << CurrentLaunch::getInstance().equation->variableNames()[i]
                      << " = " << result->solution()->getValues()[i]
                      << std::endl;
        }
    }
}

}
