#include <diophantus/parser/ParserException.h>
#include <diophantus/parser/RPNEquation.h>

#include <cctype>
#include <stack>
#include <stdexcept>
#include <string>
#include <unordered_map>

namespace Diophantus {
namespace Parser {

class Tokenizer {
    std::string expr_;
    std::size_t pos_;
    std::vector<std::string> varNames_;
    std::unordered_map<std::string, int> varNameMap_;

public:
    Tokenizer(std::string expr) : expr_(expr), pos_(0) { }
    
    const std::vector<std::string>& getVariableNames() const {
        return varNames_;   
    }

    TokenPtr nextToken() {
        while (pos_ < expr_.size() && std::isspace(expr_[pos_]))
            pos_++;
        if (pos_ == expr_.size())
            return nullptr;
        if (std::isdigit(expr_[pos_])) {
            std::string number;
            while (pos_ < expr_.size() && std::isdigit(expr_[pos_])) {
                number.push_back(expr_[pos_]);
                pos_++;
            }
            try {
                return std::make_shared<ConstantToken>(std::stoi(number));
            } catch (std::exception& e) {
                throw ParserException(expr_, "error parsing number from \"" + number + "\": " + e.what());
            }
        }
        if (std::isalpha(expr_[pos_])) {
            std::string varName;
            while (pos_ < expr_.size() && std::isalpha(expr_[pos_])) {
                varName.push_back(expr_[pos_]);
                pos_++;
            }
            if (!varNameMap_.count(varName)) {
                varNameMap_[varName] = varNames_.size();
                varNames_.push_back(varName);
            }
            return std::make_shared<VariableToken>(varNameMap_[varName]);
        }
        char token = expr_[pos_];
        pos_++;
        switch(token) {
            case '+':
                return std::make_shared<PlusOperator>();
            case '-':
                return std::make_shared<MinusOperator>();
            case '*':
                return std::make_shared<MultiplicationOperator>();
            case '(':
                return std::make_shared<LeftParen>();
            case ')':
                return std::make_shared<RightParen>();
            default:
                std::string tokenStr;
                tokenStr.push_back(token);
                throw ParserException(expr_, "unknown token '" + tokenStr + "'");
        }
    }
};

RPNEquation::RPNEquation(std::string input) {
    Tokenizer tokenizer(input);

    std::stack<TokenPtr> stack;
    TokenPtr token;
    while (token = tokenizer.nextToken()) {
        if (token->tokenType() == Token::Type::VALUE) {
            rpn_.push_back(token);
        } else if (token->tokenType() == Token::Type::OPERATOR) {
            while (!stack.empty() && stack.top()->tokenType() == Token::Type::OPERATOR && stack.top()->operatorPriority() <= token->operatorPriority()) {
                rpn_.push_back(stack.top());
                stack.pop();
            }
            stack.push(token);
        } else if (token->tokenType() == Token::Type::LEFT_PAREN) {
            stack.push(token);
        } else if (token->tokenType() == Token::Type::RIGHT_PAREN) {
            while (!stack.empty() && stack.top()->tokenType() != Token::Type::LEFT_PAREN) {
                rpn_.push_back(stack.top());
                stack.pop();
            }
            if (stack.empty())
                throw ParserException(input, "mismatched parenthesis");
            stack.pop();
        }
    }

    while (!stack.empty()) {
        if (stack.top()->tokenType() == Token::Type::LEFT_PAREN)
            throw ParserException(input, "mismatched parenthesis");
        rpn_.push_back(stack.top());
        stack.pop();
    }

    variableNames_ = tokenizer.getVariableNames();
}

}
}
