#include <diophantus/parser/Token.h>

#include <vector>
#include <stack>

namespace Diophantus {
namespace Parser {

void ConstantToken::evaluate(std::stack<int>& curStack, const std::vector<int>&) const {
    curStack.push(value_);
}

Token::Type ConstantToken::tokenType() const {
    return Token::Type::VALUE;
}

void VariableToken::evaluate(std::stack<int>& curStack, const std::vector<int>& varValues) const {
    curStack.push(varValues[id_]);
}

Token::Type VariableToken::tokenType() const {
    return Token::Type::VALUE;
}

void PlusOperator::evaluate(std::stack<int>& curStack, const std::vector<int>&) const {
    int a = curStack.top();
    curStack.pop();
    int b = curStack.top();
    curStack.pop();
    curStack.push(a + b);
}

Token::Type PlusOperator::tokenType() const {
    return Token::Type::OPERATOR;
}

int PlusOperator::operatorPriority() const {
    return 2;
}

void MinusOperator::evaluate(std::stack<int>& curStack, const std::vector<int>&) const {
    int a = curStack.top();
    curStack.pop();
    int b = curStack.top();
    curStack.pop();
    curStack.push(b - a);
}

Token::Type MinusOperator::tokenType() const {
    return Token::Type::OPERATOR;
}

int MinusOperator::operatorPriority() const {
    return 2;
}

void MultiplicationOperator::evaluate(std::stack<int>& curStack, const std::vector<int>&) const {
    int a = curStack.top();
    curStack.pop();
    int b = curStack.top();
    curStack.pop();
    curStack.push(a * b);
}

Token::Type MultiplicationOperator::tokenType() const {
    return Token::Type::OPERATOR;
}

int MultiplicationOperator::operatorPriority() const {
    return 1;
}

Token::Type LeftParen::tokenType() const {
    return Token::Type::LEFT_PAREN;
}

Token::Type RightParen::tokenType() const {
    return Token::Type::RIGHT_PAREN;
}

}
}
