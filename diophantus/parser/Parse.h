#pragma once

#include <diophantus/Equation.h>

#include <string>

namespace Diophantus {
namespace Parser {

EquationPtr Parse(std::string input);

}
}
