#pragma once

#include <vector>
#include <stack>
#include <memory>

namespace Diophantus {
namespace Parser {

class Token {
public:
    enum class Type {
        VALUE,
        OPERATOR,
        LEFT_PAREN,
        RIGHT_PAREN
    };

    virtual ~Token() {}

    virtual void evaluate(std::stack<int>& curStack, const std::vector<int>& varValues) const = 0;
    
    virtual int operatorPriority() const {
        return -1;
    }

    virtual Type tokenType() const = 0;
};

typedef std::shared_ptr<Token> TokenPtr;

class ConstantToken : public Token {
    int value_;
public:
    ConstantToken(int value) : value_(value) {}
    virtual ~ConstantToken() {}

    void evaluate(std::stack<int>& curStack, const std::vector<int>&) const override final;
    Token::Type tokenType() const override final;

    // Needed for testing
    inline int getValue() const {
        return value_;
    }
};

class VariableToken : public Token {
    int id_;
public:
    VariableToken(int id) : id_(id) {}
    virtual ~VariableToken() {}

    void evaluate(std::stack<int>& curStack, const std::vector<int>& varValues) const override final;
    Token::Type tokenType() const override final;

    // Needed for testing
    inline int getId() const {
        return id_;
    }
};

class PlusOperator : public Token {
public:
    virtual ~PlusOperator() {}

    void evaluate(std::stack<int>& curStack, const std::vector<int>&) const override final;
    Token::Type tokenType() const override final;
    int operatorPriority() const override final;
};

class MinusOperator : public Token {
public:
    virtual ~MinusOperator() {}

    void evaluate(std::stack<int>& curStack, const std::vector<int>&) const override final;
    Token::Type tokenType() const override final;
    int operatorPriority() const override final;
};

class MultiplicationOperator : public Token {
public:
    virtual ~MultiplicationOperator() {}

    void evaluate(std::stack<int>& curStack, const std::vector<int>&) const override final;
    Token::Type tokenType() const override final;
    int operatorPriority() const override final;
};

class LeftParen : public Token {
public:
    virtual ~LeftParen() {}

    inline void evaluate(std::stack<int>&, const std::vector<int>&) const override final {}
    Token::Type tokenType() const override final;
};

class RightParen : public Token {
public:
    virtual ~RightParen() {}

    inline void evaluate(std::stack<int>&, const std::vector<int>&) const override final {}
    Token::Type tokenType() const override final;
};

}
}
