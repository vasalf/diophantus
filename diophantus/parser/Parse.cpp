#include <diophantus/parser/Parse.h>
#include <diophantus/parser/ParserException.h>
#include <diophantus/parser/RPNEquation.h>

namespace Diophantus {
namespace Parser {

class ParsedEquation : public Equation {
    std::string expr_;
    RPNEquation equation_;
public:
    ParsedEquation(std::string input) : expr_(input), equation_(input) {}

    virtual ~ParsedEquation() = default;

    virtual int variableNumber() const override final {
        return equation_.getVariableNames().size();
    }

    virtual const std::vector<std::string>& variableNames() const override final {
        return equation_.getVariableNames();
    }

    virtual int evaluate(const std::vector<int>& values) const override final {
        std::stack<int> curStack;

        for (const auto& token : equation_.getRPN()) {
            token->evaluate(curStack, values);
        }

        if (curStack.size() != 1) {
            throw ParserException(expr_, "invalid operator usage");
        }
        return curStack.top();
    }
};

EquationPtr Parse(std::string input) {
    auto ret = std::make_shared<ParsedEquation>(input);

    // This will throw an exception in case of incorrect operator usage.
    ret->evaluate(std::vector<int>(input.size(), 0));
    
    return ret;
}

}
}
