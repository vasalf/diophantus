#pragma once

#include <stdexcept>
#include <string>

namespace Diophantus {
namespace Parser {

class ParserException : public std::invalid_argument {
public:
    ParserException(std::string input, std::string reason) 
        : std::invalid_argument("Could not parse \"" + input + "\": " + reason) {}
};

}
}
