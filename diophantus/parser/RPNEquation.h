#pragma once

#include <diophantus/parser/Token.h>

#include <string>
#include <vector>

namespace Diophantus {
namespace Parser {

class RPNEquation {
    std::vector<TokenPtr> rpn_;
    std::vector<std::string> variableNames_;

public:
    RPNEquation(std::string input);

    inline std::vector<TokenPtr> getRPN() const {
        return rpn_;
    }

    inline int getVariableNumber() const {
        return variableNames_.size();
    }

    inline const std::vector<std::string>& getVariableNames() const {
        return variableNames_;
    }
};

}
}
