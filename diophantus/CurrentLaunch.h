#pragma once

#include <diophantus/Equation.h>
#include <diophantus/Logger.h>

namespace Diophantus {

class CurrentLaunch {
public:
    EquationPtr equation;
    LoggerPtr logger;
private:
    CurrentLaunch() {}
    CurrentLaunch(const CurrentLaunch&) = delete;
    CurrentLaunch& operator=(const CurrentLaunch&) = delete;
public:
    static CurrentLaunch& getInstance();
};

}
