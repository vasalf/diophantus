#pragma once

#include <memory>
#include <string>
#include <vector>

namespace Diophantus {

class Equation {
public:
    virtual ~Equation() = default;
    virtual int variableNumber() const = 0;
    virtual const std::vector<std::string>& variableNames() const = 0;
    virtual int evaluate(const std::vector<int>& values) const = 0;
};

typedef std::shared_ptr<Equation> EquationPtr;

}
