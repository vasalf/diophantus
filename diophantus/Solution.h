#pragma once

#include <diophantus/CurrentLaunch.h>
#include <diophantus/Equation.h>

#include <helicase/lib/BitString.h>

#include <cmath>
#include <memory>
#include <vector>

namespace Diophantus {

class Solution {
    std::vector<int> values_;

public:
    Solution(std::vector<int> values) : values_(values) {}
    Solution(const Helicase::BitString& binaryGenotype);

    virtual ~Solution() = default;

    inline int getValue() const {
        return CurrentLaunch::getInstance().equation->evaluate(values_);
    }

    inline double getFitness() const {
        return 1.0 / std::abs(getValue());
    }

    inline std::vector<int> getValues() const {
        return values_;
    }

    Helicase::BitString getBinaryGenotype() const;
    int getBinaryGenotypeSize() const;
};

typedef std::shared_ptr<Solution> SolutionPtr;

}
