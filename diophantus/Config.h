#pragma once

#include <diophantus/Mode.h>

#include <iostream>
#include <stdexcept>
#include <string>

namespace Diophantus {

class ConfigException : public std::runtime_error {
public:
    ConfigException(std::string message) : std::runtime_error(message) {}
};

ModePtr parseConfig(std::istream& config);

}
