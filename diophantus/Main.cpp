#include <diophantus/parser/ParserException.h>
#include <diophantus/Config.h>
#include <diophantus/Mode.h>

#include <iostream>
#include <fstream>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        std::cerr << "usage: diophantus <CONFIG-FILE>" << std::endl;
        return 1;
    }
    std::ifstream config(argv[1]);
    if (!config) {
        std::cerr << "could not open config file for reading" << std::endl;
        return 1;
    }

    Diophantus::ModePtr mode;
    try {
        mode = Diophantus::parseConfig(config);
    } catch (Diophantus::ConfigException& e) {
        std::cerr << e.what() << std::endl;
        return 1;
    } catch (Diophantus::Parser::ParserException& e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    mode->runMode();

    return 0;
}
