#include <diophantus/mutation/NormalRandom.h>

#include <diophantus/CurrentLaunch.h>

#include <cmath>

namespace Diophantus {
namespace Mutation {

NormalRandom::NormalRandom(int rndSeed, double deviation) : dist_(0, deviation) {
    if (rndSeed == 0)
        rndSeed = std::random_device()();
    rnd_ = std::mt19937(rndSeed);
}

SolutionPtr NormalRandom::affect(SolutionPtr solution) {
    int vars = CurrentLaunch::getInstance().equation->variableNumber();
    std::vector<int> values(vars);

    for (int i = 0; i < vars; i++) {
        values[i] = solution->getValues()[i] + std::round(dist_(rnd_));
    }

    return std::make_shared<Solution>(values);
}

}
}
