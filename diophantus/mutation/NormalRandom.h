#pragma once

#include <diophantus/Solution.h>

#include <helicase/mutation/MutationEngine.h>

#include <random>

namespace Diophantus {
namespace Mutation {

class NormalRandom : public Helicase::MutationEngine<Diophantus::Solution> {
    std::normal_distribution<double> dist_;
    std::mt19937 rnd_;
public:
    NormalRandom(int rndSeed, double deviation);
    virtual ~NormalRandom() {};

    virtual SolutionPtr affect(SolutionPtr solution);
};

}
}
