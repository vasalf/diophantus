#include <diophantus/CurrentLaunch.h>

namespace Diophantus {

CurrentLaunch& CurrentLaunch::getInstance() {
    static CurrentLaunch instance;
    return instance;
}

}
