#pragma once

#include <memory>
#include <vector>

namespace Diophantus {

class Solution; 
typedef std::shared_ptr<Solution> SolutionPtr;

class Logger {
    bool enabled_;
public:
    Logger(bool enabled) {
        enabled_ = enabled;
    }

    void logPopulation(std::vector<SolutionPtr> population);
};

typedef std::shared_ptr<Logger> LoggerPtr;

}
