#include <diophantus/Logger.h>
#include <diophantus/Solution.h>

#include <algorithm>
#include <cmath>
#include <iostream>

namespace Diophantus {

void Logger::logPopulation(std::vector<SolutionPtr> population) {
    if (enabled_) {
        auto best = std::min_element(population.begin(), population.end(),
            [](const SolutionPtr& lhs, const SolutionPtr& rhs) -> bool {
                return std::abs(lhs->getValue()) < std::abs(rhs->getValue());
            }
        );
        auto sum = std::accumulate(population.begin(), population.end(), 0.0,
            [](double lhs, const SolutionPtr& rhs) {
                return lhs + rhs->getFitness();
            }
        );

        std::cerr << "population (sum: " << sum << ", best value: " << (*best)->getValue() << ")" << std::endl;
    }
}

}
