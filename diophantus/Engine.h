#pragma once

#include <diophantus/Solution.h>

#include <memory>
#include <stdexcept>
#include <string>

namespace Diophantus {

class FailedInvocationException : public std::runtime_error {
public:
    FailedInvocationException() : std::runtime_error("Attempt to get solution from failed invocation") {}
};

class InvocationResult {
public:
    virtual ~InvocationResult() = default;
    virtual bool success() const = 0;
    virtual int generations() const = 0;
    virtual SolutionPtr solution() const = 0;
};

typedef std::shared_ptr<InvocationResult> InvocationResultPtr;

InvocationResultPtr invocationSuccess(SolutionPtr solution, int generations);
InvocationResultPtr invocationFail();

class Engine {
public:
    virtual ~Engine() = default;
    virtual std::string getName() const = 0;
    virtual InvocationResultPtr invoke() const = 0;
};

typedef std::shared_ptr<Engine> EnginePtr;

}
