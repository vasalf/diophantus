#pragma once

#include <diophantus/Engine.h>
#include <diophantus/Mode.h> 

namespace Diophantus {

class SolveMode : public Mode {
    EnginePtr engine_;
public:
    SolveMode(EnginePtr engine) : engine_(engine) {}
    virtual void runMode() override final;
};

}
